<?php

namespace app\controllers\api;

use Yii;

use yii\rest\ActiveController;
use yii\web\Response;

use app\models\PhonebookSearch;

/**
 * Class PhonebookController
 * @package app\controllers\api
 */
class PhonebookController extends ActiveController
{
    public $modelClass = 'app\models\Phonebook';

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['contentNegotiator']['formats']['text/html'] = Response::FORMAT_JSON;

        return $behaviors;
    }

    public function actions()
    {
        $actions = parent::actions();
        $actions['index']['prepareDataProvider'] = [$this, 'prepareDataProvider'];
        return $actions;
    }

    /**
     * Search by data
     * @return \yii\data\ActiveDataProvider
     */
    public function prepareDataProvider()
    {
        $searchModel = new PhonebookSearch();
        return $searchModel->search(Yii::$app->request->queryParams);
    }

}
