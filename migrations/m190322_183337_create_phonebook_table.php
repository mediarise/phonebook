<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%phonebook}}`.
 */
class m190322_183337_create_phonebook_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%phonebook}}', [
            'id' => $this->primaryKey(),
            'firstname' => $this->string(100),
            'lastname' => $this->string(100),
            'middlename' => $this->string(100),
            'phonenumber' => $this->string(100),
            'address' => $this->string(255)
        ]);

        $this->createIndex(
            'idx-phonebook-phonenumber',
            '{{%phonebook}}',
            'phonenumber'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex('idx-phonebook-phonenumber', '{{%phonebook}}');
        $this->dropTable('{{%phonebook}}');
    }
}
