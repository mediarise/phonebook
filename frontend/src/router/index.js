import Vue from 'vue'
import Router from 'vue-router'
import Search from '@/components/Search'
import PhonebookManager from '@/components/PhonebookManager'

Vue.use(Router)

let router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Search',
      component: Search
    },
    {
      path: '/posts-manager',
      name: 'PostsManager',
      component: PhonebookManager,
      meta: {
        requiresAuth: true
      }
    }
  ]
})

export default router
