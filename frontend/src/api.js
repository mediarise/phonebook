import axios from 'axios'

const client = axios.create({
  baseURL: 'http://localhost:8081/api/',
  json: true
})

export default {
  async execute (method, resource, data) {
    return client({
      method,
      url: resource,
      data
    }).then(req => {
      return req.data
    })
  },
  getPhonebooks () {
    return this.execute('get', '/phonebooks')
  },
  getPhonebook (id) {
    return this.execute('get', `/phonebooks/${id}`)
  },
  createPhonebook (data) {
    return this.execute('post', '/phonebooks', data)
  },
  updatePhonebook (id, data) {
    return this.execute('put', `/phonebooks/${id}`, data)
  },
  deletePhonebook (id) {
    return this.execute('delete', `/phonebooks/${id}`)
  }
}
