<?php
namespace app\tests\fixtures;

use yii\test\ActiveFixture;

class PhonebookFixture extends ActiveFixture
{
    public $modelClass = 'app\models\Phonebook';
}