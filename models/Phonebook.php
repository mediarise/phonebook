<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "phonebook".
 *
 * @property int $id
 * @property string $firstname
 * @property string $lastname
 * @property string $middlename
 * @property string $phonenumber
 * @property string $address
 */
class Phonebook extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'phonebook';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['firstname', 'lastname', 'middlename', 'phonenumber'], 'string', 'max' => 100],
            [['address'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'firstname' => Yii::t('app', 'Firstname'),
            'lastname' => Yii::t('app', 'Lastname'),
            'middlename' => Yii::t('app', 'Middlename'),
            'phonenumber' => Yii::t('app', 'Phonenumber'),
            'address' => Yii::t('app', 'Address'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return PhonebookQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PhonebookQuery(get_called_class());
    }
}
